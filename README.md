# SpringDeveloper Angular

####**Angular frontend for SpringDeveloper.**

Warning: **client only**. 
  Use REST API from backend [spring-eveloper-rest project](https://bitbucket.org/nnikitaFrolov/springdeveloperrest/overview)
  You need start backend server before start frontend application.
  

## Installation
1. Update angular-cli to latest version (1.1 current)
as described on [angular-cli github readme.md](https://github.com/angular/angular-cli#updating-angular-cli)

````
npm uninstall -g angular-cli @angular/cli
npm cache clean
npm install -g @angular/cli@latest
````
Clone project from github
````
git clone https://nnikitaFrolov@bitbucket.org/nnikitaFrolov/springdeveloperangular.git
````
Install local project package
````
npm install --save-dev @angular/cli@latest
if npm version > 5.0 delete package-lock.json file  ( bug - this file prevent correct packages install)
npm install
````

Now project use Angular CLI v.1.1 and Angular v.4.1.
You can see current dependencies in package.json file.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.