import { DeveloperAngularPage } from './app.po';

describe('developer-angular App', () => {
  let page: DeveloperAngularPage;

  beforeEach(() => {
    page = new DeveloperAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
