
import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {DeveloperListComponent} from "./developer-list/developer-list.component";
import {DeveloperEditComponent} from "./developer-edit/developer-edit.component";
import {DeveloperAddComponent} from "./developer-add/developer-add.component";

const developerRoutes: Routes = [
  {path: 'developers', component: DeveloperListComponent},
  {path: 'developer/add', component: DeveloperAddComponent},
  {
    path: 'developer/:id',
    children: [
      {
        path: 'edit',
        component: DeveloperEditComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(developerRoutes)],
  exports: [RouterModule]
})
export class DeveloperRoutingModule {
}
