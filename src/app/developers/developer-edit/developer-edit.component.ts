import { Component, OnInit } from '@angular/core';
import {Developer} from "../developer";
import {ActivatedRoute, Router} from "@angular/router";
import {DeveloperService} from "../developer.service";

@Component({
  selector: 'app-developer-edit',
  templateUrl: './developer-edit.component.html',
  styleUrls: ['./developer-edit.component.css']
})
export class DeveloperEditComponent implements OnInit {
  developer: Developer;
  errorMessage: string;

  constructor(private developerService: DeveloperService, private router: Router, private route: ActivatedRoute) {
    this.developer = <Developer>{};
  }

  ngOnInit() {
    const developerId = this.route.snapshot.params['id'];
    this.developerService.getDeveloperById(developerId).subscribe(
      developer => {
        this.developer = developer;
      },
      error => this.errorMessage = <any> error
    );
  }

  onSubmit(developer: Developer) {
    developer.id = this.developer.id;
    this.developerService.updateDeveloper(developer.id, developer).subscribe(
      get_result,
      error => this.errorMessage = <any> error
    );
    function get_result(update_status) {
      if (update_status.status === 200) {
        return console.log('update success');
      } else {
        return console.log('update failed');
      }
    }
  }
}
