import {UUID} from "angular2-uuid";
export  class Developer{
  id: UUID;
  name: string;
  salary: number
}
