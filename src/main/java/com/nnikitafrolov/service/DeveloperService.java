package com.nnikitafrolov.service;

import com.nnikitafrolov.model.Developer;

import java.util.List;
import java.util.UUID;

public interface DeveloperService {
    Developer findById(UUID id);

    Developer findByName(String name);

    void saveDeveloper(Developer developer);

    void updateDeveloper(Developer developer);

    void deleteDeveloperById(UUID id);

    void deleteAllDevelopers();

    List<Developer> findAllDevelopers();

    boolean isDeveloperExist(Developer developer);
}
